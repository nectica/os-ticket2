<?php 
require_once INCLUDE_DIR . 'class.organization.php';
require_once INCLUDE_DIR . 'class.staff.php';
class EmailCustom {

    public static function sendAlertOverTime($org_id = null, $staff = null){
        
        if ($org_id) {
            $organization = Organization::lookup($org_id);
           
            self::CheckOrgTime($organization,  $staff);
            
        } else {
            foreach (Organization::objects() as $organization) {                
                self::CheckOrgTime($organization,  $staff);
               
            }
        }
        return true;
    }
    // get time logged on each ticket in currently Month
    public  static function getTimeOrg($org_id){
        $now = new DateTime();
        try {
            $sql = 'SELECT  SUM(thEntry.Time) as time from ost_ticket ti 
                    inner Join ost_thread th on ti.ticket_id = th.object_id
                    INNER JOIN  ost_thread_entry thEntry on thEntry.thread_id = th.id 
                    INNER Join ost_user user on user.id  = ti.user_id 
                    WHERE user.org_id ='.$org_id.' AND  MONTH(thEntry.created) = '.$now->format('m').' AND YEAR(thEntry.created)= '.$now->format('Y');
                    $db =new PDO("mysql:host=localhost;dbname=".DBNAME, DBUSER, DBPASS);
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
        } catch (\Exception $th) {
            echo $th->getMessage();
        }
        return  $stmt->fetch();
    }
    //send email to staffs
    public static function sendAlert($organization, $diff,  $staff){
        if ($diff < 0) {  
            if($staff != null and $staff != false ){
                self::sendEmail( $organization->getName(),$staff->getEmail() );
            } else {
                foreach (Staff::objects() as $staff) {
                    self::sendEmail( $organization->getName(),$staff->getEmail() );
                }    
            }
       } 
    }
    public static function  sendEmail($company, $emailStaff){
        global $cfg;
        $email = $cfg->getDefaultEmail();
        $body = "La empresa ".$company." ya alcanzado la cantidad de hora contratada por mes.";
        $email->send( $emailStaff,' alerta de horas consumidas ', $body );
    }
    // check if org has over time this month
    public static function  CheckOrgTime($organization,  $staff){
        $logTime = Self::getTimeOrg($organization->getId());
        $diff =( $organization->cdata->workTime * 60) - $logTime['time']   ;
        self::sendAlert($organization, $diff,  $staff);  
    }


}

?>