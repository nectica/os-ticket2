<?php
 header('Access-control-allow-origin: *');
 header("Content-type: application/json; charset=UTF-8");
 header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
 class Response  {
    private $response ;
	public function __construct($response){
		$this->response = $response;
	}
	public static function json($response, $statusCode = 202){
		http_response_code($statusCode);
		echo  json_encode($response, JSON_UNESCAPED_UNICODE) ;
	}
 }
?>
