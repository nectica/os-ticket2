<?php
require_once '../core/Response.php';
require_once './../../config.inc.php';
class OrgApi {

	public static function getAllOrg(){
		$today = new DateTime();
		$sql = 'SELECT org.id as id,org.name AS nombre, data.workTime AS hrsCont FROM ost_organization AS org
		INNER JOIN  ost_organization__cdata AS data ON org.id = data.org_id ORDER BY hrsCont';


		$db = new PDO("mysql:host=localhost;dbname=".DBNAME,DBUSER, DBPASS ,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		$smt = $db->prepare($sql);
		$smt->execute();
		$orgs = array();
		$aux = $smt->fetchAll(PDO::FETCH_ASSOC);
		foreach($aux as $org){
			$org['hrsCont'] =  $org['hrsCont'] ? : 0;
			$timeByMoth = self::getTimeOrg($org['id']);
			if ($org['hrsCont'] == 99999) {
				$org['hrsCont'] = $timeByMoth['hrsConsumidas'];
			}
			$org = array_merge($org, $timeByMoth  ) ;
			array_push($orgs,$org);
		}
		usort($orgs, function($org1, $org2){
			return $org1['hrsCont'] > $org2['hrsCont'];
		});
		Response::json($orgs);
	}
	// get time logged on each ticket in currently Month
    public  static function getTimeOrg($org_id){
        $now = new DateTime();
        try {
            $sql = 'SELECT   TRUNCATE(SUM(thEntry.Time )/60, 2)  as hrsConsumidas from ost_ticket ti
                    inner Join ost_thread th on ti.ticket_id = th.object_id
                    INNER JOIN  ost_thread_entry thEntry on thEntry.thread_id = th.id
                    INNER Join ost_user user on user.id  = ti.user_id
                    WHERE user.org_id ='.$org_id.' AND  MONTH(thEntry.created) = '.$now->format('m').' AND YEAR(thEntry.created)= '.$now->format('Y');
                    $db =new PDO("mysql:host=localhost;dbname=".DBNAME, DBUSER, DBPASS,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
        } catch (\Exception $th) {
            echo $th->getMessage();
        }
		$hrs =  $stmt->fetch(PDO::FETCH_ASSOC);
		$hrs['hrsConsumidas'] = $hrs['hrsConsumidas'] ?:0;
        return $hrs;
    }

	public static function getOrgSelected($orgs_id){
		$today = new DateTime();
		$sql = "SELECT org.id as id,org.name AS nombre, data.workTime AS hrsCont FROM ost_organization AS org
		INNER JOIN  ost_organization__cdata AS data ON org.id = data.org_id
		WHERE org.id IN ($orgs_id)";


		$db = new PDO("mysql:host=localhost;dbname=".DBNAME,DBUSER, DBPASS,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		$smt = $db->prepare($sql);
		$smt->execute();
		$orgs = array();
		$aux = $smt->fetchAll(PDO::FETCH_ASSOC);
		foreach($aux as $org){
			$org['hrsCont'] =  $org['hrsCont'] ? : 0;
			$timeByMoth = self::getTimeOrg($org['id']);

			$org = array_merge($org,  $timeByMoth) ;
			if ($org['hrsCont'] == 99999) {
				$org['hrsCont'] = $timeByMoth['hrsConsumidas'];
			}
			array_push($orgs,$org);
		}
		usort($orgs, function($org1, $org2){
			return $org1['hrsCont'] > $org2['hrsCont'];
		});
		Response::json($orgs);
	}
}
switch($_SERVER['REQUEST_METHOD']){
	case 'GET':
		OrgApi::getAllOrg();
	break;
	case 'POST':
		OrgApi::getOrgSelected( $_POST['orgs']);
	break;

}


/* 		LEFT JOIN ost_user user on user.org_id = org.id
		LEFT JOIN ost_ticket ti on ti.user_id = user.id
		LEFT JOIN ost_thread th on ti.ticket_id = th.object_id
        LEFT JOIN  ost_thread_entry thEntry on thEntry.thread_id = th.id
		WHERE  MONTH(thEntry.created) = '.$today->format('m').' AND YEAR(thEntry.created)= '.$today->format('Y').'
		GROUP BY org.id'; */
