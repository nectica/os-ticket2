<?php
require_once '../core/Response.php';
require_once './../../config.inc.php';
class StaffApi {
	public static function getAllStaff(){
		try {
			$sql = "SELECT staff.staff_id, CONCAT(staff.firstname, ' ', staff.lastname) as nombre
			FROM ost_staff as staff
			";

			$db = new PDO("mysql:host=localhost;dbname=".DBNAME, DBUSER, DBPASS ,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$staffs = array();
			$allStaff= $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($allStaff as $staff){
				$timeByMoth = self::getTimestaffByMonth($staff['staff_id']);
				$aux = array_merge($staff, $timeByMoth);
				array_push($staffs, $aux);
			}

		} catch (\Exception $th) {
			echo $th->getMessage();
		}
		usort($staffs,function($staff1, $staff2){
			return $staff1['horas'] > $staff2['horas'];
		 });
		Response::json($staffs);
	}

	public static function getTimestaffByMonth($staffId){
		$today = new DateTime();
		try {
			$sql = "SELECT TRUNCATE( SUM(thread.Time) /60 , 2 )  as horas FROM ost_thread_entry as thread
			WHERE thread.staff_id = ".$staffId." AND  MONTH(thread.created) = ".$today->format('m')." AND YEAR(thread.created) = ".$today->format('Y') ;
			$db = new PDO("mysql:host=localhost;dbname=".DBNAME, DBUSER, DBPASS ,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$timeByMoth = $stmt->fetch(PDO::FETCH_ASSOC);
			$timeByMoth['horas']  = $timeByMoth['horas'] ?: 0;
			return $timeByMoth;
		} catch (\Exception $th) {
			echo $th->getMessage();

		}
	}
	public static function getStaffsById($staffIds){
		try {
			$sql = "SELECT staff.staff_id, CONCAT(staff.firstname, ' ', staff.lastname) as nombre
			FROM ost_staff as staff
			WHERE staff.staff_id IN ($staffIds)
			GROUP BY staff.staff_id";

			$db = new PDO("mysql:host=localhost;dbname=".DBNAME, DBUSER, DBPASS , array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$staffs = array();
			$allStaff= $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($allStaff as $staff){
				$timeByMoth = self::getTimestaffByMonth($staff['staff_id']);
				$aux = array_merge($staff, $timeByMoth);
				array_push($staffs, $aux);

			}

		} catch (\Exception $th) {
			echo $th->getMessage();
		}
		usort($staffs,function($staff1, $staff2){
			return $staff1['horas'] > $staff2['horas'];
		 });
		Response::json($staffs);
	}
}


switch($_SERVER['REQUEST_METHOD']){
	case 'GET':
		StaffApi::getAllStaff();
	break;
	case 'POST':
		StaffApi::getStaffsById($_POST['staffIds']);
	break;
}
