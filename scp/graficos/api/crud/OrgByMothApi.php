<?php
require_once '../core/Response.php';
require_once './../../config.inc.php';

class OrgByMothApi extends Response {
	public static function getAllOrg(){
		$sql = 'SELECT org.id as id,org.name AS nombre FROM ost_organization AS org ORDER BY nombre';


		$db = new PDO("mysql:host=localhost;dbname=".DBNAME,DBUSER, DBPASS ,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		$smt = $db->prepare($sql);
		$smt->execute();
		$orgs = $smt->fetchAll(PDO::FETCH_ASSOC);

		self::json($orgs);
	}



	public  static function getOrgByMoths($org_id){
        $now = new DateTime();
		$oneYearAgo = new DateInterval('P1Y');
		$now->sub($oneYearAgo);
		$now->setTime(0,0);
        try {
            $sql = "SELECT  org.id as id,org.name AS nombre, data.workTime AS hrsCont,
						TRUNCATE(SUM(Entry.Time )/60, 2)  as hrsConsumidas,
						CONCAT(MONTHNAME(Entry.created), ' ', YEAR(Entry.created))  as fecha, MONTH(Entry.created) as mes
					FROM ost_organization AS org
					INNER JOIN  ost_organization__cdata AS data ON org.id = data.org_id
					LEFT  JOIN ost_user AS user ON user.org_id = org.id
					LEFT JOIN ost_ticket AS ticket on ticket.user_id = user.id
					LEFT JOIN ost_thread thread on ticket.ticket_id = thread.object_id
					LEFT JOIN  ost_thread_entry Entry on Entry.thread_id = thread.id
					WHERE org.id = ".$org_id." AND Entry.created >=". $now->format('Y-m-d')  ."
					GROUP By org.id, fecha";
            $db =new PDO("mysql:host=localhost;dbname=".DBNAME, DBUSER, DBPASS,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $stmt = $db->prepare($sql);
            $stmt->execute();
        } catch (\Exception $th) {
            echo $th->getMessage();
        }

		$orgByLast12Months =  $stmt->fetchAll(PDO::FETCH_ASSOC);

		self::checkHoursByOrgs($orgByLast12Months );
       	self::json($orgByLast12Months) ;
    }
	public  static function checkHoursByOrgs(&$orgs){
		foreach($orgs as  $index =>$org){
			if ($org['hrsCont'] == 99999) {
				$orgs[$index]['hrsCont'] = $org['hrsConsumidas'];
			}
		}
		return $orgs;
	}

}
switch ($_SERVER['REQUEST_METHOD']) {
	case 'GET':
        $now = new DateTime();
		$oneYearAgo = new DateInterval('P1Y');
		$now->sub($oneYearAgo);
		$now->setTime(0,0);

		OrgByMothApi::getAllOrg();
		break;
	case 'POST':
		OrgByMothApi::getOrgByMoths($_POST['org_id']);
		break;

}
