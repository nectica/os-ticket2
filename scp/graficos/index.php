<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./graficos/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./graficos/devExpress/lib/css/dx.common.css " />
	<link rel="stylesheet" href="./graficos/devExpress/lib/css/dx.light.css" />
</head>

<body class="min-vh-100">
	<div class="container-fluid pt-5 min-vh-100">
		<div class=" row" style="min-height:80vh;">
			<div class="col-8" id="chart">
				<h1>Graficos de horas.(Mes Actual)</h1>
			</div>
			<div class="col-4 form-group" id="checkboxs-orgs">
			</div>
		</div>
		<div class=" row" style="min-height:80vh;">
			<div class="col-8" id="charByMonth">
				<h1>Graficos de horas de empresas por meses.</h1>
			</div>
			<div class="col-4 form-group" id="radio-month-orgs">
			</div>
		</div>
		<div class="row mt-5" style="min-height:40vh;">
			<div class="col-8" id="staffId">
			</div>
			<div class="col-4 form-group" id="checkboxs-staffs">

			</div>
		</div>
	</div>
	<script type="text/javascript" src="./graficos/devExpress/lib/js/jquery-2.2.3.js"></script>
	<script type="text/javascript" src="./graficos/devExpress/lib/js/dx.viz-web.js"></script>
	<script type="text/javascript" src="./graficos/devExpress/lib/js/globalize.min.js"></script>
	<script type="text/javascript" src="./graficos/devExpress/lib/js/jszip.js"></script>
	<script type="text/javascript">
		const $j= jQuery.noConflict();
		$j(document).ready(function(){
			let idJobsetTimeout = null;
			const fetchAllOrg = () => {
				const orgAll = fetch('./graficos/api/crud/OrgApi.php');
				orgAll.then( response => response.json())
						.then(loadAllOrg);
			}
			const loadAllOrg = (orgs) =>{
				renderAllOrgs(orgs);
				loadCheckboxsOrg(orgs);

			}
			const renderAllOrgs = (orgs) =>{
				for (const org of orgs) {
					org.hrsConsumidas = parseFloat(org.hrsConsumidas);
					org.hrsCont = parseFloat(org.hrsCont);
				}
				$j("#chart").dxChart({
					dataSource: orgs,
					rotated: true,
					commonSeriesSettings: {
						argumentField: "nombre",
						type: "bar",
						hoverMode: "allArgumentPoints",
						selectionMode: "allArgumentPoints",
						label: {
							visible: true,
							format: {
								type: "fixedPoint",
								precision: 2
							}
						}
					},
					series: [
						{valueField: "hrsConsumidas", name: "Horas consumidas."},
						{valueField: "hrsCont", name: "Horas contratadas"},
					],
					title: "Clientes: Horas contratadas por Mes Vs Horas usadas Por mes ",
					legend: {
						verticalAlignment: "bottom",
						horizontalAlignment: "center"
					},

					"export": {
						enabled: true
					},
					onPointClick: function(e) {

						e.target.select();
					}
				});
			}

			const loadCheckboxsOrg = (orgs) =>{
				const container = document.querySelector('#checkboxs-orgs');
				container.innerHTML = '';
				for (const org of orgs) {
					const containerCheck = document.createElement('div');
					containerCheck.classList.add('form-check');
					const check = document.createElement('input');
					check.classList.add('form-check-input');
					check.setAttribute('type', 'checkbox');
					check.setAttribute('name', 'org');
					check.setAttribute('value', org.id);
					check.addEventListener('change',changeHandlerOrg);

					containerCheck.appendChild(check);
					const label = document.createElement('label');
					label.classList.add('form-check-label');
					label.appendChild(document.createTextNode(org.nombre));
					containerCheck.appendChild(label);
					container.appendChild(containerCheck);

				}
				const containerForm= document.createElement('div');
				containerForm.classList.add('form-check', 'form-check-inline' );
				const resetButton = document.createElement('button');
				resetButton.classList.add('btn','btn-success');
				resetButton.setAttribute('type','button');
				resetButton.appendChild(document.createTextNode('Mostrar todos'));
				resetButton.addEventListener('click', ()=>{
					 uncheckedList ('org', fetchAllOrg);

				});
				containerForm.appendChild(resetButton);
				container.appendChild(containerForm);


			}
			const changeHandlerOrg = (evt) =>{
				clearTimeout(idJobsetTimeout);
				idJobsetTimeout =  setTimeout(() =>{
					fetchOrgSelected();
					return true;
				}, 1500);
			}
			const fetchOrgSelected = () => {
				const orgsSelected = document.querySelectorAll('input[name=org]:checked');
				const orgs = [];
				for (const org of orgsSelected) {
					orgs.push(org.value)
				}
				if(orgs.length){
					const formData = new FormData();
					formData.append('orgs',  orgs);
					const fetchOrgById = fetch('./graficos/api/crud/OrgApi.php',{
						method: 'POST',
						body: formData,
					}).then(response => response.json())
					.then( json => {
						renderAllOrgs(json);

					} );
				}else {
					fetchAllOrg();
				}
			}
			const fetchAllStaffs = () => {
				fetch('./graficos/api/crud/StaffApi.php')
				.then(response => response.json())
				.then( loadStaff );

			}
			const loadStaff = (staffs) =>{
				RenderAllStaffs(staffs);
				loadCheckboxsStaff(staffs);
			}
			const RenderAllStaffs = (staffs) => {
				for (const staff of staffs) {
					staff.horas = parseFloat(staff.horas);
				}
				$j("#staffId").dxChart({
					dataSource: staffs,
					commonSeriesSettings: {
						argumentField: "nombre",
						type: "bar",
						hoverMode: "allArgumentPoints",
						selectionMode: "allArgumentPoints",
						label: {
							visible: true,
							format: {
								type: "fixedPoint",
								precision: 0
							}
						}
					},
					rotated: true,

					series: [{
							valueField: "horas",
							name: "Horas Consumidas"
						},

					],
					title: "Horas Consumidas por agentes de soporte ",
					legend: {
						verticalAlignment: "bottom",
						horizontalAlignment: "center"
					},

					"export": {
						enabled: true
					},
					onPointClick: function(e) {
						e.target.select();
					}
				});
			}
			const loadCheckboxsStaff = (staffs) =>{
				const container = document.querySelector('#checkboxs-staffs');
				container.innerHTML = '';
				for (const staff of staffs) {
					const containerCheck = document.createElement('div');
					containerCheck.classList.add('form-check' );
					const check = document.createElement('input');
					check.classList.add('form-check-input');
					check.setAttribute('type', 'checkbox');
					check.setAttribute('name', 'staff');
					check.setAttribute('value', staff.staff_id);
					check.addEventListener('change',changeHandlerStaff);

					containerCheck.appendChild(check);
					const label = document.createElement('label');
					label.classList.add('form-check-label');
					label.appendChild(document.createTextNode(staff.nombre));
					containerCheck.appendChild(label);
					container.appendChild(containerCheck);
				}
				const containerForm= document.createElement('div');
				containerForm.classList.add('form-check', 'form-check-inline' );
				const resetButton = document.createElement('button');
				resetButton.classList.add('btn','btn-success');
				resetButton.setAttribute('type','button');
				resetButton.appendChild(document.createTextNode('Mostrar todos'));
				resetButton.addEventListener('click', ()=>{
					 uncheckedList ('staff', fetchAllStaffs);

				});
				containerForm.appendChild(resetButton);
				container.appendChild(containerForm);
			}
			const uncheckedList = (name, callbackShowAll) => {
				const checkboxes = document.querySelectorAll(`input[name=${name}]:checked`);
				for (const checkbox of checkboxes) {
					checkbox.checked = false;
				}
				callbackShowAll();
			}
			let idJobsetTimeoutStaff = null;
			const changeHandlerStaff = (evt) => {
				clearTimeout(idJobsetTimeoutStaff);
				setTimeout( ()=>{
					fetchStaffsSelected();
					return true;
				} ,1500);

			}
			const fetchStaffsSelected = () => {
				const staffs = document.querySelectorAll('input[name=staff]:checked');
				const formData = new FormData();
				const staffsId =  Array.from(staffs).map( staff => staff.value);
				formData.append('staffIds', staffsId);
				if(staffs.length){
					fetch('./graficos/api/crud/StaffApi.php',
						{method: 'POST',
						   body: formData,}
						).then(response => response.json())
						.then(RenderAllStaffs)
						   ;

				}else {
					fetchAllStaffs();
				}
			}

			const fecthAllOrgRadio = () => {
				const fetchAllOrg = fetch('./graficos/api/crud/OrgByMothApi.php');
				fetchAllOrg.then(response => response.json())
				.then(loadRadio);
			}
			const loadRadio = (orgs) => {
				const container =  document.querySelector('#radio-month-orgs');
				for (const org of orgs) {
					const containerRadio = document.createElement('div');
					containerRadio.classList.add('form-check');
					const input = document.createElement('input');
					input.classList.add('form-check-input');
					input.setAttribute('type', 'radio');
					input.setAttribute('name', 'org-radio');
					input.setAttribute('value', org.id);
					input.addEventListener('change',hadlerChangeRadio);
					containerRadio.appendChild(input);
					const label = document.createElement('label');
					label.appendChild(document.createTextNode(org.nombre));
					label.classList.add('form-check-label');
					containerRadio.appendChild(label);
					container.appendChild(containerRadio);

				}
			}
			const hadlerChangeRadio = (evt) =>{
				const formData = new FormData();
				formData.append('org_id', evt.target.value);
				const fetchOrgByMoth = fetch('./graficos/api/crud/OrgByMothApi.php',{method: 'POST',
						   body: formData,});
				fetchOrgByMoth.then(response => response.json()).then(renderChartOrgByMonths);
			}
			const renderChartOrgByMonths = (orgs) =>{
				for (const org of orgs) {
					org.hrsConsumidas = parseFloat(org.hrsConsumidas);
					org.hrsCont = parseFloat(org.hrsCont);
				}
				$j("#charByMonth").dxChart({
					dataSource: orgs,
					rotated: true,
					commonSeriesSettings: {
						argumentField: "fecha",
						type: "bar",
						hoverMode: "allArgumentPoints",
						selectionMode: "allArgumentPoints",
						label: {
							visible: true,
							format: {
								type: "fixedPoint",
								precision: 2
							}
						}
					},
					series: [
						{valueField: "hrsConsumidas", name: "Horas consumidas."},
						{valueField: "hrsCont", name: "Horas contratadas"},
					],
					title: "Clientes: Horas contratadas por Mes Vs Horas usadas Por mes ",
					legend: {
						verticalAlignment: "bottom",
						horizontalAlignment: "center"
					},

					"export": {
						enabled: true
					},
					onPointClick: function(e) {

						e.target.select();
					}
				});
			}
			fecthAllOrgRadio();
			fetchAllOrg();
			fetchAllStaffs();
		});
	</script>
</body>

</html>
